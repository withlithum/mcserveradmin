# ServerAdmin

简易服务器管理 GUI。

## 使用

### 导入自定义服务端文件

将服务端的全部文件（除了 `server.properties`）放置在主程序目录下的 `server` 目录下。注意在此阶段，本软件还不能安装插件。

### 导入自定义存档

1. 在服务器关闭状态下，点击 **服务器(S)** -> **管理存档(M)**：

   ![ManageSaves](Imgs/ManageSaves.png)

2. 选择“导入存档”：

   ![Import](Imgs/Import.png)

3. 选择存档文件路径后导入。导入后关闭窗口，再打开窗口（切勿再次导入），此时应该能够看到版本信息等。

若出现其它错误提示，或者是需要安装服务端，请前往 Wiki 查询更多信息。

## 下载和安装

### 预览版

1. 打开[自动构建](https://ci.appveyor.com/project/RelaperCrystal/mcserveradmin/build/artifacts)页面。
2. 选择最下方的文件名，通常为 `ServerAdmin\bin\ServerAdmin-Release.zip`。
3. 等待下载完成。
4. 解压到任意目录运行。