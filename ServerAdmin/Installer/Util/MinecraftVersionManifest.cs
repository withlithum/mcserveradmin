﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;

namespace ServerAdmin.Installer.Util
{
    internal class MinecraftVersionManifest
    {
        [JsonProperty("latest")]
        public VersionPointer LatestVersion { get; set; }
        [JsonProperty("versions")]
        public List<MinecraftVersion> Versions { get; set; }
    }

    public class MinecraftVersion
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public string InformationUrl { get; set; }
        [JsonProperty("time")]
        public DateTime BuiltTime { get; set; }
        [JsonProperty("releaseTime")]
        public DateTime ReleaseTime { get; set; }
    }

#pragma warning disable CA1815 // Override equals and operator equals on value types
    public struct VersionPointer
#pragma warning restore CA1815 // Override equals and operator equals on value types
    {
        [JsonProperty("release")]
        public string LatestRelease { get; set; }
        [JsonProperty("snapshot")]
        public string LatestSnapshot { get; set; }
    }
}
