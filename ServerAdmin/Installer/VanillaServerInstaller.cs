﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkUI.Controls;
using DarkUI.Forms;
using Newtonsoft.Json;
using ServerAdmin.Installer.Util;

namespace ServerAdmin.Installer
{
    public partial class VanillaServerInstaller : DarkForm
    {
        private readonly WebClient client = new WebClient();
        private int stage;
        private MinecraftVersionManifest manifest;

        public VanillaServerInstaller()
        {
            InitializeComponent();
        }

        private void VanillaServerInstaller_Load(object sender, EventArgs e)
        {
            client.DownloadStringAsync(new Uri("https://launchermeta.mojang.com/mc/game/version_manifest.json"));
            client.DownloadStringCompleted += Client_DownloadStringCompleted;
        }

        private void Client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (stage == 0)
            {
                if (e.Cancelled)
                {
                    MessageBox.Show(e.Error.ToString());
                    Close();
                    return;
                }
                stage++;

                labelStatus.Text = "解析版本信息中";

                manifest = JsonConvert.DeserializeObject<MinecraftVersionManifest>(e.Result);
                foreach (var item in manifest.Versions)
                {
                    if (item.Type == "old_beta" || item.Type == "old_alpha" || item.Type == "snapshot" || item.Id == "1.16.4" || item.Id.StartsWith("1.5.") || item.Id.StartsWith("1.4.") || item.Id.StartsWith("1.3.") || item.Id.StartsWith("1.2.") || item.Id == "1.1" || item.Id.StartsWith("1.0"))
                    {
                        Debug.WriteLine("Filtered out version " + item.Id);
                        continue;
                    }
                    Debug.WriteLine("Processing version " + item.Type + " " + item.Id);
                    var dropItem = new DarkDropdownItem(item.Id);
                    listVersion.Items.Add(dropItem);
                }
                labelStatus.Text = "就绪";
                progressBar1.Style = ProgressBarStyle.Continuous;
                buttonInstall.Enabled = true;
                return;
            }
        }

        private void VanillaServerInstaller_FormClosed(object sender, FormClosedEventArgs e)
        {
            client.Dispose();
        }

        private void ButtonInstall_Click(object sender, EventArgs e)
        {
            foreach (var item in manifest.Versions)
            {
                if (item.Id == listVersion.SelectedItem.Text)
                {
                    buttonInstall.Enabled = false;
                    listVersion.Enabled = false;
                    labelStatus.Text = "正在下载服务端";
                    client.DownloadFileAsync(new Uri($"https://bmclapi2.bangbang93.com/version/{item.Id}/server"), "server\\server.jar");
                    client.DownloadProgressChanged += Client_DownloadProgressChanged;
                    client.DownloadFileCompleted += Client_DownloadFileCompleted;
                }
            }
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            File.WriteAllText("server\\eula.txt", "eula=true");
            MessageBox.Show("安装完成。");
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            labelStatus.Text = $"正在下载服务端 ({e.BytesReceived / 1024 / 1024}MB / {e.TotalBytesToReceive / 1024 / 1024}MB)";
        }
    }
}
