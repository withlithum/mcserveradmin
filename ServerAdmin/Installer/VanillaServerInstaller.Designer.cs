﻿
namespace ServerAdmin.Installer
{
    partial class VanillaServerInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonInstall = new DarkUI.Controls.DarkButton();
            this.labelStatus = new DarkUI.Controls.DarkLabel();
            this.darkLabel1 = new DarkUI.Controls.DarkLabel();
            this.listVersion = new DarkUI.Controls.DarkDropdownList();
            this.darkLabel2 = new DarkUI.Controls.DarkLabel();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 136);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(742, 10);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 0;
            // 
            // buttonInstall
            // 
            this.buttonInstall.Enabled = false;
            this.buttonInstall.Location = new System.Drawing.Point(606, 154);
            this.buttonInstall.Name = "buttonInstall";
            this.buttonInstall.Padding = new System.Windows.Forms.Padding(5);
            this.buttonInstall.Size = new System.Drawing.Size(124, 32);
            this.buttonInstall.TabIndex = 1;
            this.buttonInstall.Text = "安装";
            this.buttonInstall.Click += new System.EventHandler(this.ButtonInstall_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.labelStatus.Location = new System.Drawing.Point(12, 158);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(166, 24);
            this.labelStatus.TabIndex = 2;
            this.labelStatus.Text = "正在获取版本列表...";
            // 
            // darkLabel1
            // 
            this.darkLabel1.AutoSize = true;
            this.darkLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel1.Location = new System.Drawing.Point(12, 9);
            this.darkLabel1.Name = "darkLabel1";
            this.darkLabel1.Size = new System.Drawing.Size(64, 24);
            this.darkLabel1.TabIndex = 3;
            this.darkLabel1.Text = "版本号";
            // 
            // listVersion
            // 
            this.listVersion.Location = new System.Drawing.Point(82, 7);
            this.listVersion.Name = "listVersion";
            this.listVersion.Size = new System.Drawing.Size(172, 30);
            this.listVersion.TabIndex = 4;
            this.listVersion.Text = "darkDropdownList1";
            // 
            // darkLabel2
            // 
            this.darkLabel2.AutoSize = true;
            this.darkLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel2.Location = new System.Drawing.Point(12, 109);
            this.darkLabel2.Name = "darkLabel2";
            this.darkLabel2.Size = new System.Drawing.Size(568, 24);
            this.darkLabel2.TabIndex = 5;
            this.darkLabel2.Text = "获取版本列表时出现卡顿、无响应为正常现象，请耐心等待一会即可！";
            // 
            // VanillaServerInstaller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 196);
            this.Controls.Add(this.darkLabel2);
            this.Controls.Add(this.listVersion);
            this.Controls.Add(this.darkLabel1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonInstall);
            this.Controls.Add(this.progressBar1);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VanillaServerInstaller";
            this.Text = "原版服务端安装器";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VanillaServerInstaller_FormClosed);
            this.Load += new System.EventHandler(this.VanillaServerInstaller_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private DarkUI.Controls.DarkButton buttonInstall;
        private DarkUI.Controls.DarkLabel labelStatus;
        private DarkUI.Controls.DarkLabel darkLabel1;
        private DarkUI.Controls.DarkDropdownList listVersion;
        private DarkUI.Controls.DarkLabel darkLabel2;
    }
}