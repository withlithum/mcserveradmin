﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerAdmin
{
    internal static class FormExtensions
    {
        internal static void Flash(this Form form)
        {
            var fw = new FLASHWINFO
            {
                cbSize = Convert.ToUInt32(Marshal.SizeOf(typeof(FLASHWINFO))),
                hwnd = form.Handle,
                dwFlags = 2,
                uCount = 3
            };

            FlashWindowEx(ref fw);
        }

        [DllImport("user32.dll")]
        private static extern int FlashWindowEx(ref FLASHWINFO pwfi);
    }

    [StructLayout(LayoutKind.Sequential)]
#pragma warning disable S101 // Types should be named in PascalCase
#pragma warning disable CA1815 // Override equals and operator equals on value types
    public struct FLASHWINFO
#pragma warning restore CA1815 // Override equals and operator equals on value types
#pragma warning restore S101 // Types should be named in PascalCase
    {
        public UInt32 cbSize;
        public IntPtr hwnd;
        public Int32 dwFlags;
        public UInt32 uCount;
        public Int32 dwTimeout;
    }
}
