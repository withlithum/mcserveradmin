﻿
namespace ServerAdmin
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.buttonSubmit = new DarkUI.Controls.DarkButton();
            this.darkContextMenu1 = new DarkUI.Controls.DarkContextMenu();
            this.darkMenuStrip1 = new DarkUI.Controls.DarkMenuStrip();
            this.itemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.itemQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.itemServer = new System.Windows.Forms.ToolStripMenuItem();
            this.itemInstall = new System.Windows.Forms.ToolStripMenuItem();
            this.itemVanilla = new System.Windows.Forms.ToolStripMenuItem();
            this.itemPaper = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSaveManager = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCommand = new System.Windows.Forms.ToolStripMenuItem();
            this.itemStopCommand = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSeperator1 = new System.Windows.Forms.ToolStripSeparator();
            this.itemAutoSave = new System.Windows.Forms.ToolStripMenuItem();
            this.itemEnableAutoSave = new System.Windows.Forms.ToolStripMenuItem();
            this.itemDisableAutoSave = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.itemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.itemReportBug = new System.Windows.Forms.ToolStripMenuItem();
            this.textOutput = new DarkUI.Controls.DarkTextBox();
            this.buttonStart = new DarkUI.Controls.DarkButton();
            this.labelInput = new DarkUI.Controls.DarkLabel();
            this.darkTextBox1 = new DarkUI.Controls.DarkTextBox();
            this.buttonForceStop = new DarkUI.Controls.DarkButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.itemSeperator2 = new System.Windows.Forms.ToolStripSeparator();
            this.itemImportServer = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonDebugFlash = new DarkUI.Controls.DarkButton();
            this.darkMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tooltip
            // 
            this.tooltip.AutoPopDelay = 25000;
            this.tooltip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.tooltip.ForeColor = System.Drawing.Color.Gainsboro;
            this.tooltip.InitialDelay = 500;
            this.tooltip.ReshowDelay = 100;
            // 
            // buttonSubmit
            // 
            resources.ApplyResources(this.buttonSubmit, "buttonSubmit");
            this.buttonSubmit.Name = "buttonSubmit";
            this.tooltip.SetToolTip(this.buttonSubmit, resources.GetString("buttonSubmit.ToolTip"));
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // darkContextMenu1
            // 
            this.darkContextMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.darkContextMenu1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkContextMenu1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.darkContextMenu1.Name = "darkContextMenu1";
            resources.ApplyResources(this.darkContextMenu1, "darkContextMenu1");
            // 
            // darkMenuStrip1
            // 
            this.darkMenuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.darkMenuStrip1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkMenuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.darkMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.darkMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemFile,
            this.itemServer,
            this.itemCommand,
            this.itemHelp});
            resources.ApplyResources(this.darkMenuStrip1, "darkMenuStrip1");
            this.darkMenuStrip1.Name = "darkMenuStrip1";
            // 
            // itemFile
            // 
            this.itemFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemSettings,
            this.itemQuit});
            this.itemFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemFile.Name = "itemFile";
            resources.ApplyResources(this.itemFile, "itemFile");
            // 
            // itemSettings
            // 
            this.itemSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemSettings.Name = "itemSettings";
            resources.ApplyResources(this.itemSettings, "itemSettings");
            this.itemSettings.Click += new System.EventHandler(this.itemSettings_Click);
            // 
            // itemQuit
            // 
            this.itemQuit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemQuit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemQuit.Name = "itemQuit";
            resources.ApplyResources(this.itemQuit, "itemQuit");
            this.itemQuit.Click += new System.EventHandler(this.itemQuit_Click);
            // 
            // itemServer
            // 
            this.itemServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemServer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemInstall,
            this.itemSaveManager});
            this.itemServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemServer.Name = "itemServer";
            resources.ApplyResources(this.itemServer, "itemServer");
            // 
            // itemInstall
            // 
            this.itemInstall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemInstall.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemVanilla,
            this.itemPaper,
            this.itemSeperator2,
            this.itemImportServer});
            this.itemInstall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemInstall.Name = "itemInstall";
            resources.ApplyResources(this.itemInstall, "itemInstall");
            // 
            // itemVanilla
            // 
            this.itemVanilla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemVanilla.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemVanilla.Name = "itemVanilla";
            resources.ApplyResources(this.itemVanilla, "itemVanilla");
            this.itemVanilla.Click += new System.EventHandler(this.itemVanilla_Click);
            // 
            // itemPaper
            // 
            this.itemPaper.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            resources.ApplyResources(this.itemPaper, "itemPaper");
            this.itemPaper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.itemPaper.Name = "itemPaper";
            // 
            // itemSaveManager
            // 
            this.itemSaveManager.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemSaveManager.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemSaveManager.Name = "itemSaveManager";
            resources.ApplyResources(this.itemSaveManager, "itemSaveManager");
            this.itemSaveManager.Click += new System.EventHandler(this.itemSaveManager_Click);
            // 
            // itemCommand
            // 
            this.itemCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemCommand.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemStopCommand,
            this.itemSeperator1,
            this.itemAutoSave,
            this.itemSaveAll});
            this.itemCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemCommand.Name = "itemCommand";
            resources.ApplyResources(this.itemCommand, "itemCommand");
            // 
            // itemStopCommand
            // 
            this.itemStopCommand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemStopCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemStopCommand.Name = "itemStopCommand";
            resources.ApplyResources(this.itemStopCommand, "itemStopCommand");
            this.itemStopCommand.Click += new System.EventHandler(this.itemStopCommand_Click);
            // 
            // itemSeperator1
            // 
            this.itemSeperator1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemSeperator1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemSeperator1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.itemSeperator1.Name = "itemSeperator1";
            resources.ApplyResources(this.itemSeperator1, "itemSeperator1");
            // 
            // itemAutoSave
            // 
            this.itemAutoSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemAutoSave.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemEnableAutoSave,
            this.itemDisableAutoSave});
            this.itemAutoSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemAutoSave.Name = "itemAutoSave";
            resources.ApplyResources(this.itemAutoSave, "itemAutoSave");
            // 
            // itemEnableAutoSave
            // 
            this.itemEnableAutoSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemEnableAutoSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemEnableAutoSave.Name = "itemEnableAutoSave";
            resources.ApplyResources(this.itemEnableAutoSave, "itemEnableAutoSave");
            this.itemEnableAutoSave.Click += new System.EventHandler(this.itemEnableAutoSave_Click);
            // 
            // itemDisableAutoSave
            // 
            this.itemDisableAutoSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemDisableAutoSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemDisableAutoSave.Name = "itemDisableAutoSave";
            resources.ApplyResources(this.itemDisableAutoSave, "itemDisableAutoSave");
            this.itemDisableAutoSave.Click += new System.EventHandler(this.itemDisableAutoSave_Click);
            // 
            // itemSaveAll
            // 
            this.itemSaveAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemSaveAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemSaveAll.Name = "itemSaveAll";
            resources.ApplyResources(this.itemSaveAll, "itemSaveAll");
            this.itemSaveAll.Click += new System.EventHandler(this.itemSaveAll_Click);
            // 
            // itemHelp
            // 
            this.itemHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemAbout,
            this.itemReportBug});
            this.itemHelp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemHelp.Name = "itemHelp";
            resources.ApplyResources(this.itemHelp, "itemHelp");
            // 
            // itemAbout
            // 
            this.itemAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemAbout.Name = "itemAbout";
            resources.ApplyResources(this.itemAbout, "itemAbout");
            this.itemAbout.Click += new System.EventHandler(this.itemAbout_Click);
            // 
            // itemReportBug
            // 
            this.itemReportBug.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemReportBug.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemReportBug.Name = "itemReportBug";
            resources.ApplyResources(this.itemReportBug, "itemReportBug");
            this.itemReportBug.Click += new System.EventHandler(this.itemReportBug_Click);
            // 
            // textOutput
            // 
            this.textOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textOutput, "textOutput");
            this.textOutput.Name = "textOutput";
            this.textOutput.ReadOnly = true;
            // 
            // buttonStart
            // 
            resources.ApplyResources(this.buttonStart, "buttonStart");
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelInput
            // 
            resources.ApplyResources(this.labelInput, "labelInput");
            this.labelInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.labelInput.Name = "labelInput";
            // 
            // darkTextBox1
            // 
            this.darkTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.darkTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.darkTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.darkTextBox1, "darkTextBox1");
            this.darkTextBox1.Name = "darkTextBox1";
            // 
            // buttonForceStop
            // 
            resources.ApplyResources(this.buttonForceStop, "buttonForceStop");
            this.buttonForceStop.Name = "buttonForceStop";
            this.buttonForceStop.Click += new System.EventHandler(this.buttonForceStop_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // itemSeperator2
            // 
            this.itemSeperator2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemSeperator2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemSeperator2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.itemSeperator2.Name = "itemSeperator2";
            resources.ApplyResources(this.itemSeperator2, "itemSeperator2");
            // 
            // itemImportServer
            // 
            this.itemImportServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.itemImportServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.itemImportServer.Name = "itemImportServer";
            resources.ApplyResources(this.itemImportServer, "itemImportServer");
            this.itemImportServer.Click += new System.EventHandler(this.itemImportServer_Click);
            // 
            // buttonDebugFlash
            // 
            resources.ApplyResources(this.buttonDebugFlash, "buttonDebugFlash");
            this.buttonDebugFlash.Name = "buttonDebugFlash";
            this.buttonDebugFlash.Click += new System.EventHandler(this.buttonDebugFlash_Click);
            // 
            // MainFrm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonDebugFlash);
            this.Controls.Add(this.buttonForceStop);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.darkTextBox1);
            this.Controls.Add(this.labelInput);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.textOutput);
            this.Controls.Add(this.darkMenuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.darkMenuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainFrm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.darkMenuStrip1.ResumeLayout(false);
            this.darkMenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip tooltip;
        private DarkUI.Controls.DarkContextMenu darkContextMenu1;
        private DarkUI.Controls.DarkMenuStrip darkMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem itemFile;
        private System.Windows.Forms.ToolStripMenuItem itemSettings;
        private System.Windows.Forms.ToolStripMenuItem itemQuit;
        private System.Windows.Forms.ToolStripMenuItem itemServer;
        private System.Windows.Forms.ToolStripMenuItem itemInstall;
        private System.Windows.Forms.ToolStripMenuItem itemVanilla;
        private System.Windows.Forms.ToolStripMenuItem itemPaper;
        private DarkUI.Controls.DarkTextBox textOutput;
        private DarkUI.Controls.DarkButton buttonStart;
        private DarkUI.Controls.DarkLabel labelInput;
        private DarkUI.Controls.DarkTextBox darkTextBox1;
        private DarkUI.Controls.DarkButton buttonSubmit;
        private System.Windows.Forms.Timer timer1;
        private DarkUI.Controls.DarkButton buttonForceStop;
        private System.Windows.Forms.ToolStripMenuItem itemHelp;
        private System.Windows.Forms.ToolStripMenuItem itemAbout;
        private System.Windows.Forms.ToolStripMenuItem itemSaveManager;
        private System.Windows.Forms.ToolStripMenuItem itemCommand;
        private System.Windows.Forms.ToolStripMenuItem itemStopCommand;
        private System.Windows.Forms.ToolStripMenuItem itemAutoSave;
        private System.Windows.Forms.ToolStripMenuItem itemEnableAutoSave;
        private System.Windows.Forms.ToolStripMenuItem itemReportBug;
        private System.Windows.Forms.ToolStripMenuItem itemDisableAutoSave;
        private System.Windows.Forms.ToolStripSeparator itemSeperator1;
        private System.Windows.Forms.ToolStripMenuItem itemSaveAll;
        private System.Windows.Forms.ToolStripSeparator itemSeperator2;
        private System.Windows.Forms.ToolStripMenuItem itemImportServer;
        private DarkUI.Controls.DarkButton buttonDebugFlash;
    }
}

