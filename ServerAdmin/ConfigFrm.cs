﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkUI.Forms;
using Kajabity.Tools.Java;
using ServerAdmin.Texts;

namespace ServerAdmin
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:命名样式", Justification = "<挂起>")]
    public partial class ConfigFrm : DarkForm
    {
        public ConfigFrm()
        {
            InitializeComponent();
        }

        private void buttonSaveProperties_Click(object sender, EventArgs e)
        {
            var properties = new JavaProperties
            {
                ["online-mode"] = checkOnlineMode.Checked.ToString().ToLower(),
                ["white-list"] = checkWhitelist.Checked.ToString().ToLower(),
                ["spawn-protection"] = textSpawnProtection.Text,
                ["difficulty"] = comboDifficulty.Text,
                ["gamemode"] = comboGamemode.Text,
                ["player-idle-timeout"] = numericIdle.Value.ToString(),
                ["pvp"] = checkPvP.Checked.ToString().ToLower(),
                ["motd"] = textMotd.Text,
                ["spawn-monsters"] = checkMonsters.Checked.ToString().ToLower(),
                ["spawn-animals"] = checkAnimals.Checked.ToString().ToLower(),
                ["spawn-npcs"] = checkVillagers.Checked.ToString().ToLower(),
                ["max-players"] = numericPlayerLimit.Value.ToString(),
                ["max-tick-time"] = numericMaxTick.Value.ToString(),
                ["broadcast-console-to-ops"] = checkBroadcastConsole.Checked.ToString().ToLower(),
                ["allow-nether"] = checkNether.Checked.ToString().ToLower(),
                ["enforce-whitelist"] = checkEnforceWhitelist.Checked.ToString().ToLower(),
                ["entity-broadcast-range-percentage"] = numericEntity.Value.ToString(),
                ["prevent-proxy-connections"] = checkAllowProxy.Checked.ToString().ToLower(),
                ["server-ip"] = textIP.Text,
                ["server-port"] = textPort.Text,
                ["snooper-enabled"] = darkSnooper.Checked.ToString().ToLower()
            };

            if (!int.TryParse(textSpawnProtection.Text, out _))
            {
                toolTip1.Show("此值必须为整数。", textSpawnProtection, 3000);
                return;
            }

            using (var stream = File.Create("server\\server.properties"))
            {
                properties.Store(stream, false);
            }
            MessageBox.Show("保存成功");
        }

        public static long GetTotalPhysicalMemory()
        {
            long capacity = 0;
            try
            {
                foreach (ManagementObject mo1 in new ManagementClass("Win32_PhysicalMemory").GetInstances().OfType<ManagementObject>())
                    capacity += long.Parse(mo1.Properties["Capacity"].Value.ToString());
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
            {
                capacity = -1;
                Console.WriteLine(ex.Message);
            }
#pragma warning restore CA1031 // Do not catch general exception types
            return capacity;
        }

        private void ConfigFrm_Load(object sender, EventArgs e)
        {
            if (!File.Exists("server\\server.properties"))
            {
                comboGamemode.SelectedIndex = 0;
                comboDifficulty.SelectedIndex = 2;
                checkOnlineMode.Checked = true;
                checkWhitelist.Checked = false;
            }
            else
            {
                var properties = new JavaProperties();
                using (var stream = File.OpenRead("server\\server.properties"))
                {
                    properties.Load(stream);
                }
                try
                {
                    comboGamemode.Text = properties["gamemode"];
                    comboDifficulty.Text = properties["difficulty"];
                    textSpawnProtection.Text = properties["spawn-protection"];
                    checkOnlineMode.Checked = bool.Parse(properties["online-mode"]);
                    checkWhitelist.Checked = bool.Parse(properties["white-list"]);
                    numericIdle.Value = int.Parse(properties["player-idle-timeout"]);
                    checkPvP.Checked = bool.Parse(properties["pvp"]);
                    checkMonsters.Checked = bool.Parse(properties["spawn-monsters"]);
                    checkAnimals.Checked = bool.Parse(properties["spawn-animals"]);
                    checkVillagers.Checked = bool.Parse(properties["spawn-npcs"]);

                    checkAllowProxy.Checked = bool.Parse(properties["prevent-proxy-connections"]);
                    checkBroadcastConsole.Checked = bool.Parse(properties["broadcast-console-to-ops"]);
                    checkEnforceWhitelist.Checked = bool.Parse(properties["enforce-whitelist"]);
                    checkNether.Checked = bool.Parse(properties["allow-nether"]);
                    textIP.Text = properties["server-ip"];
                    textPort.Text = properties["server-port"];
                    numericMaxTick.Value = int.Parse(properties["max-tick-time"]);
                    numericPlayerLimit.Value = int.Parse(properties["max-players"]);
                    numericEntity.Value = int.Parse(properties["entity-broadcast-range-percentage"]);
                    darkSnooper.Checked = bool.Parse(properties["snooper-enabled"]);

                    textMotd.Text = properties["motd"];
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch (NullReferenceException)
                {
                    File.Delete("server\\server.properties");
                    MessageBox.Show(DialogText.IncompactibleServerPropertiesText, DialogText.GenericError, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Close();
                }
#pragma warning restore CA1031 // Do not catch general exception types
#pragma warning disable CA1031 // Do not catch general exception types
                catch (KeyNotFoundException)
                {
                    File.Delete("server\\server.properties");
                    MessageBox.Show(DialogText.IncompactibleServerPropertiesText, DialogText.GenericError, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Close();
                }
#pragma warning restore CA1031 // Do not catch general exception types
            }
            numericMaxMemory.Maximum = GetTotalPhysicalMemory();
            numericMaxMemory.Value = Properties.Settings.Default.MaxMemory;
            textJarFile.Text = Properties.Settings.Default.ServerJarName;
            textJavaExecutable.Text = Properties.Settings.Default.JavaExecutable;
            toolTip1.SetToolTip(textJarFile, "指示服务器的主执行文件的文件名（带扩展名），即启动服务器所用的 jar 文件。\r\n例如，执行 java -jar server.jar 会启动服务器，那么服务器 Jar 文件则是 server.jar。");
        }

        private void buttonSaveLaunch_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.MaxMemory = (int)numericMaxMemory.Value;
            Properties.Settings.Default.ServerJarName = textJarFile.Text;
            Properties.Settings.Default.JavaExecutable = textJavaExecutable.Text;
            Properties.Settings.Default.Save();
        }
    }
}
