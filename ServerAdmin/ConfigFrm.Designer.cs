﻿
namespace ServerAdmin
{
    partial class ConfigFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigFrm));
            this.darkGroupBox1 = new DarkUI.Controls.DarkGroupBox();
            this.textJavaExecutable = new DarkUI.Controls.DarkTextBox();
            this.buttonGetJavaPath = new DarkUI.Controls.DarkButton();
            this.darkLabel3 = new DarkUI.Controls.DarkLabel();
            this.numericMaxMemory = new DarkUI.Controls.DarkNumericUpDown();
            this.darkLabel2 = new DarkUI.Controls.DarkLabel();
            this.textJarFile = new DarkUI.Controls.DarkTextBox();
            this.darkLabel1 = new DarkUI.Controls.DarkLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.darkSnooper = new DarkUI.Controls.DarkCheckBox();
            this.comboGamemode = new DarkUI.Controls.DarkComboBox();
            this.numericIdle = new DarkUI.Controls.DarkNumericUpDown();
            this.checkBroadcastConsole = new DarkUI.Controls.DarkCheckBox();
            this.checkEnforceWhitelist = new DarkUI.Controls.DarkCheckBox();
            this.numericEntity = new DarkUI.Controls.DarkNumericUpDown();
            this.checkNether = new DarkUI.Controls.DarkCheckBox();
            this.checkAllowProxy = new DarkUI.Controls.DarkCheckBox();
            this.textIP = new DarkUI.Controls.DarkTextBox();
            this.textPort = new DarkUI.Controls.DarkTextBox();
            this.numericMaxTick = new DarkUI.Controls.DarkNumericUpDown();
            this.numericPlayerLimit = new DarkUI.Controls.DarkNumericUpDown();
            this.darkGroupBox2 = new DarkUI.Controls.DarkGroupBox();
            this.labelIdle = new DarkUI.Controls.DarkLabel();
            this.textSpawnProtection = new DarkUI.Controls.DarkTextBox();
            this.darkLabel4 = new DarkUI.Controls.DarkLabel();
            this.checkWhitelist = new DarkUI.Controls.DarkCheckBox();
            this.checkOnlineMode = new DarkUI.Controls.DarkCheckBox();
            this.darkGroupBox3 = new DarkUI.Controls.DarkGroupBox();
            this.darkLabel8 = new DarkUI.Controls.DarkLabel();
            this.textMotd = new DarkUI.Controls.DarkTextBox();
            this.darkLabel7 = new DarkUI.Controls.DarkLabel();
            this.checkPvP = new DarkUI.Controls.DarkCheckBox();
            this.checkVillagers = new DarkUI.Controls.DarkCheckBox();
            this.checkAnimals = new DarkUI.Controls.DarkCheckBox();
            this.checkMonsters = new DarkUI.Controls.DarkCheckBox();
            this.comboDifficulty = new DarkUI.Controls.DarkComboBox();
            this.darkLabel6 = new DarkUI.Controls.DarkLabel();
            this.darkLabel5 = new DarkUI.Controls.DarkLabel();
            this.buttonSaveProperties = new DarkUI.Controls.DarkButton();
            this.buttonSaveLaunch = new DarkUI.Controls.DarkButton();
            this.darkGroupBox4 = new DarkUI.Controls.DarkGroupBox();
            this.darkLabel12 = new DarkUI.Controls.DarkLabel();
            this.darkLabel11 = new DarkUI.Controls.DarkLabel();
            this.darkLabel10 = new DarkUI.Controls.DarkLabel();
            this.darkLabel9 = new DarkUI.Controls.DarkLabel();
            this.darkGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxMemory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIdle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEntity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxTick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPlayerLimit)).BeginInit();
            this.darkGroupBox2.SuspendLayout();
            this.darkGroupBox3.SuspendLayout();
            this.darkGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // darkGroupBox1
            // 
            this.darkGroupBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.darkGroupBox1.Controls.Add(this.textJavaExecutable);
            this.darkGroupBox1.Controls.Add(this.buttonGetJavaPath);
            this.darkGroupBox1.Controls.Add(this.darkLabel3);
            this.darkGroupBox1.Controls.Add(this.numericMaxMemory);
            this.darkGroupBox1.Controls.Add(this.darkLabel2);
            this.darkGroupBox1.Controls.Add(this.textJarFile);
            this.darkGroupBox1.Controls.Add(this.darkLabel1);
            resources.ApplyResources(this.darkGroupBox1, "darkGroupBox1");
            this.darkGroupBox1.Name = "darkGroupBox1";
            this.darkGroupBox1.TabStop = false;
            // 
            // textJavaExecutable
            // 
            this.textJavaExecutable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textJavaExecutable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textJavaExecutable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textJavaExecutable, "textJavaExecutable");
            this.textJavaExecutable.Name = "textJavaExecutable";
            // 
            // buttonGetJavaPath
            // 
            resources.ApplyResources(this.buttonGetJavaPath, "buttonGetJavaPath");
            this.buttonGetJavaPath.Name = "buttonGetJavaPath";
            // 
            // darkLabel3
            // 
            resources.ApplyResources(this.darkLabel3, "darkLabel3");
            this.darkLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel3.Name = "darkLabel3";
            // 
            // numericMaxMemory
            // 
            resources.ApplyResources(this.numericMaxMemory, "numericMaxMemory");
            this.numericMaxMemory.Name = "numericMaxMemory";
            // 
            // darkLabel2
            // 
            resources.ApplyResources(this.darkLabel2, "darkLabel2");
            this.darkLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel2.Name = "darkLabel2";
            // 
            // textJarFile
            // 
            this.textJarFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textJarFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textJarFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textJarFile, "textJarFile");
            this.textJarFile.Name = "textJarFile";
            this.toolTip1.SetToolTip(this.textJarFile, resources.GetString("textJarFile.ToolTip"));
            // 
            // darkLabel1
            // 
            resources.ApplyResources(this.darkLabel1, "darkLabel1");
            this.darkLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel1.Name = "darkLabel1";
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toolTip1.ForeColor = System.Drawing.SystemColors.HighlightText;
            // 
            // darkSnooper
            // 
            resources.ApplyResources(this.darkSnooper, "darkSnooper");
            this.darkSnooper.Name = "darkSnooper";
            this.toolTip1.SetToolTip(this.darkSnooper, resources.GetString("darkSnooper.ToolTip"));
            // 
            // comboGamemode
            // 
            this.comboGamemode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.comboGamemode.FormattingEnabled = true;
            this.comboGamemode.Items.AddRange(new object[] {
            resources.GetString("comboGamemode.Items"),
            resources.GetString("comboGamemode.Items1"),
            resources.GetString("comboGamemode.Items2")});
            resources.ApplyResources(this.comboGamemode, "comboGamemode");
            this.comboGamemode.Name = "comboGamemode";
            this.toolTip1.SetToolTip(this.comboGamemode, resources.GetString("comboGamemode.ToolTip"));
            // 
            // numericIdle
            // 
            resources.ApplyResources(this.numericIdle, "numericIdle");
            this.numericIdle.Name = "numericIdle";
            this.toolTip1.SetToolTip(this.numericIdle, resources.GetString("numericIdle.ToolTip"));
            // 
            // checkBroadcastConsole
            // 
            resources.ApplyResources(this.checkBroadcastConsole, "checkBroadcastConsole");
            this.checkBroadcastConsole.Checked = true;
            this.checkBroadcastConsole.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBroadcastConsole.Name = "checkBroadcastConsole";
            this.toolTip1.SetToolTip(this.checkBroadcastConsole, resources.GetString("checkBroadcastConsole.ToolTip"));
            // 
            // checkEnforceWhitelist
            // 
            resources.ApplyResources(this.checkEnforceWhitelist, "checkEnforceWhitelist");
            this.checkEnforceWhitelist.Name = "checkEnforceWhitelist";
            this.toolTip1.SetToolTip(this.checkEnforceWhitelist, resources.GetString("checkEnforceWhitelist.ToolTip"));
            // 
            // numericEntity
            // 
            resources.ApplyResources(this.numericEntity, "numericEntity");
            this.numericEntity.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericEntity.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericEntity.Name = "numericEntity";
            this.toolTip1.SetToolTip(this.numericEntity, resources.GetString("numericEntity.ToolTip"));
            this.numericEntity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // checkNether
            // 
            resources.ApplyResources(this.checkNether, "checkNether");
            this.checkNether.Checked = true;
            this.checkNether.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkNether.Name = "checkNether";
            this.toolTip1.SetToolTip(this.checkNether, resources.GetString("checkNether.ToolTip"));
            // 
            // checkAllowProxy
            // 
            resources.ApplyResources(this.checkAllowProxy, "checkAllowProxy");
            this.checkAllowProxy.Name = "checkAllowProxy";
            this.toolTip1.SetToolTip(this.checkAllowProxy, resources.GetString("checkAllowProxy.ToolTip"));
            // 
            // textIP
            // 
            this.textIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textIP, "textIP");
            this.textIP.Name = "textIP";
            this.toolTip1.SetToolTip(this.textIP, resources.GetString("textIP.ToolTip"));
            // 
            // textPort
            // 
            this.textPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPort.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textPort, "textPort");
            this.textPort.Name = "textPort";
            this.toolTip1.SetToolTip(this.textPort, resources.GetString("textPort.ToolTip"));
            // 
            // numericMaxTick
            // 
            this.numericMaxTick.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            resources.ApplyResources(this.numericMaxTick, "numericMaxTick");
            this.numericMaxTick.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericMaxTick.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericMaxTick.Name = "numericMaxTick";
            this.toolTip1.SetToolTip(this.numericMaxTick, resources.GetString("numericMaxTick.ToolTip"));
            // 
            // numericPlayerLimit
            // 
            resources.ApplyResources(this.numericPlayerLimit, "numericPlayerLimit");
            this.numericPlayerLimit.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericPlayerLimit.Name = "numericPlayerLimit";
            this.toolTip1.SetToolTip(this.numericPlayerLimit, resources.GetString("numericPlayerLimit.ToolTip"));
            this.numericPlayerLimit.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // darkGroupBox2
            // 
            this.darkGroupBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.darkGroupBox2.Controls.Add(this.numericIdle);
            this.darkGroupBox2.Controls.Add(this.labelIdle);
            this.darkGroupBox2.Controls.Add(this.darkSnooper);
            this.darkGroupBox2.Controls.Add(this.textSpawnProtection);
            this.darkGroupBox2.Controls.Add(this.darkLabel4);
            this.darkGroupBox2.Controls.Add(this.checkWhitelist);
            this.darkGroupBox2.Controls.Add(this.checkOnlineMode);
            resources.ApplyResources(this.darkGroupBox2, "darkGroupBox2");
            this.darkGroupBox2.Name = "darkGroupBox2";
            this.darkGroupBox2.TabStop = false;
            // 
            // labelIdle
            // 
            resources.ApplyResources(this.labelIdle, "labelIdle");
            this.labelIdle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.labelIdle.Name = "labelIdle";
            // 
            // textSpawnProtection
            // 
            this.textSpawnProtection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textSpawnProtection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSpawnProtection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textSpawnProtection, "textSpawnProtection");
            this.textSpawnProtection.Name = "textSpawnProtection";
            // 
            // darkLabel4
            // 
            resources.ApplyResources(this.darkLabel4, "darkLabel4");
            this.darkLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel4.Name = "darkLabel4";
            // 
            // checkWhitelist
            // 
            resources.ApplyResources(this.checkWhitelist, "checkWhitelist");
            this.checkWhitelist.Name = "checkWhitelist";
            // 
            // checkOnlineMode
            // 
            resources.ApplyResources(this.checkOnlineMode, "checkOnlineMode");
            this.checkOnlineMode.Name = "checkOnlineMode";
            // 
            // darkGroupBox3
            // 
            this.darkGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.darkGroupBox3.Controls.Add(this.numericEntity);
            this.darkGroupBox3.Controls.Add(this.darkLabel8);
            this.darkGroupBox3.Controls.Add(this.textMotd);
            this.darkGroupBox3.Controls.Add(this.darkLabel7);
            this.darkGroupBox3.Controls.Add(this.checkPvP);
            this.darkGroupBox3.Controls.Add(this.checkVillagers);
            this.darkGroupBox3.Controls.Add(this.checkAnimals);
            this.darkGroupBox3.Controls.Add(this.checkMonsters);
            this.darkGroupBox3.Controls.Add(this.comboDifficulty);
            this.darkGroupBox3.Controls.Add(this.darkLabel6);
            this.darkGroupBox3.Controls.Add(this.comboGamemode);
            this.darkGroupBox3.Controls.Add(this.darkLabel5);
            resources.ApplyResources(this.darkGroupBox3, "darkGroupBox3");
            this.darkGroupBox3.Name = "darkGroupBox3";
            this.darkGroupBox3.TabStop = false;
            // 
            // darkLabel8
            // 
            resources.ApplyResources(this.darkLabel8, "darkLabel8");
            this.darkLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel8.Name = "darkLabel8";
            // 
            // textMotd
            // 
            this.textMotd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.textMotd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMotd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            resources.ApplyResources(this.textMotd, "textMotd");
            this.textMotd.Name = "textMotd";
            // 
            // darkLabel7
            // 
            resources.ApplyResources(this.darkLabel7, "darkLabel7");
            this.darkLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel7.Name = "darkLabel7";
            // 
            // checkPvP
            // 
            resources.ApplyResources(this.checkPvP, "checkPvP");
            this.checkPvP.Name = "checkPvP";
            // 
            // checkVillagers
            // 
            resources.ApplyResources(this.checkVillagers, "checkVillagers");
            this.checkVillagers.Checked = true;
            this.checkVillagers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkVillagers.Name = "checkVillagers";
            // 
            // checkAnimals
            // 
            resources.ApplyResources(this.checkAnimals, "checkAnimals");
            this.checkAnimals.Checked = true;
            this.checkAnimals.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAnimals.Name = "checkAnimals";
            // 
            // checkMonsters
            // 
            resources.ApplyResources(this.checkMonsters, "checkMonsters");
            this.checkMonsters.Checked = true;
            this.checkMonsters.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkMonsters.Name = "checkMonsters";
            // 
            // comboDifficulty
            // 
            this.comboDifficulty.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.comboDifficulty.FormattingEnabled = true;
            this.comboDifficulty.Items.AddRange(new object[] {
            resources.GetString("comboDifficulty.Items"),
            resources.GetString("comboDifficulty.Items1"),
            resources.GetString("comboDifficulty.Items2"),
            resources.GetString("comboDifficulty.Items3")});
            resources.ApplyResources(this.comboDifficulty, "comboDifficulty");
            this.comboDifficulty.Name = "comboDifficulty";
            // 
            // darkLabel6
            // 
            resources.ApplyResources(this.darkLabel6, "darkLabel6");
            this.darkLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel6.Name = "darkLabel6";
            // 
            // darkLabel5
            // 
            resources.ApplyResources(this.darkLabel5, "darkLabel5");
            this.darkLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel5.Name = "darkLabel5";
            // 
            // buttonSaveProperties
            // 
            resources.ApplyResources(this.buttonSaveProperties, "buttonSaveProperties");
            this.buttonSaveProperties.Name = "buttonSaveProperties";
            this.buttonSaveProperties.Click += new System.EventHandler(this.buttonSaveProperties_Click);
            // 
            // buttonSaveLaunch
            // 
            resources.ApplyResources(this.buttonSaveLaunch, "buttonSaveLaunch");
            this.buttonSaveLaunch.Name = "buttonSaveLaunch";
            this.buttonSaveLaunch.Click += new System.EventHandler(this.buttonSaveLaunch_Click);
            // 
            // darkGroupBox4
            // 
            this.darkGroupBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.darkGroupBox4.Controls.Add(this.numericPlayerLimit);
            this.darkGroupBox4.Controls.Add(this.darkLabel12);
            this.darkGroupBox4.Controls.Add(this.numericMaxTick);
            this.darkGroupBox4.Controls.Add(this.darkLabel11);
            this.darkGroupBox4.Controls.Add(this.textPort);
            this.darkGroupBox4.Controls.Add(this.darkLabel10);
            this.darkGroupBox4.Controls.Add(this.textIP);
            this.darkGroupBox4.Controls.Add(this.darkLabel9);
            this.darkGroupBox4.Controls.Add(this.checkAllowProxy);
            this.darkGroupBox4.Controls.Add(this.checkNether);
            this.darkGroupBox4.Controls.Add(this.checkEnforceWhitelist);
            this.darkGroupBox4.Controls.Add(this.checkBroadcastConsole);
            resources.ApplyResources(this.darkGroupBox4, "darkGroupBox4");
            this.darkGroupBox4.Name = "darkGroupBox4";
            this.darkGroupBox4.TabStop = false;
            // 
            // darkLabel12
            // 
            resources.ApplyResources(this.darkLabel12, "darkLabel12");
            this.darkLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel12.Name = "darkLabel12";
            // 
            // darkLabel11
            // 
            resources.ApplyResources(this.darkLabel11, "darkLabel11");
            this.darkLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel11.Name = "darkLabel11";
            // 
            // darkLabel10
            // 
            resources.ApplyResources(this.darkLabel10, "darkLabel10");
            this.darkLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel10.Name = "darkLabel10";
            // 
            // darkLabel9
            // 
            resources.ApplyResources(this.darkLabel9, "darkLabel9");
            this.darkLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.darkLabel9.Name = "darkLabel9";
            // 
            // ConfigFrm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.darkGroupBox4);
            this.Controls.Add(this.buttonSaveLaunch);
            this.Controls.Add(this.buttonSaveProperties);
            this.Controls.Add(this.darkGroupBox3);
            this.Controls.Add(this.darkGroupBox2);
            this.Controls.Add(this.darkGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigFrm";
            this.Load += new System.EventHandler(this.ConfigFrm_Load);
            this.darkGroupBox1.ResumeLayout(false);
            this.darkGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxMemory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIdle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEntity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxTick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPlayerLimit)).EndInit();
            this.darkGroupBox2.ResumeLayout(false);
            this.darkGroupBox2.PerformLayout();
            this.darkGroupBox3.ResumeLayout(false);
            this.darkGroupBox3.PerformLayout();
            this.darkGroupBox4.ResumeLayout(false);
            this.darkGroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DarkUI.Controls.DarkGroupBox darkGroupBox1;
        private DarkUI.Controls.DarkTextBox textJarFile;
        private DarkUI.Controls.DarkLabel darkLabel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private DarkUI.Controls.DarkButton buttonGetJavaPath;
        private DarkUI.Controls.DarkLabel darkLabel3;
        private DarkUI.Controls.DarkNumericUpDown numericMaxMemory;
        private DarkUI.Controls.DarkLabel darkLabel2;
        private DarkUI.Controls.DarkTextBox textJavaExecutable;
        private DarkUI.Controls.DarkGroupBox darkGroupBox2;
        private DarkUI.Controls.DarkNumericUpDown numericIdle;
        private DarkUI.Controls.DarkLabel labelIdle;
        private DarkUI.Controls.DarkCheckBox darkSnooper;
        private DarkUI.Controls.DarkTextBox textSpawnProtection;
        private DarkUI.Controls.DarkLabel darkLabel4;
        private DarkUI.Controls.DarkCheckBox checkWhitelist;
        private DarkUI.Controls.DarkCheckBox checkOnlineMode;
        private DarkUI.Controls.DarkGroupBox darkGroupBox3;
        private DarkUI.Controls.DarkCheckBox checkPvP;
        private DarkUI.Controls.DarkCheckBox checkVillagers;
        private DarkUI.Controls.DarkCheckBox checkAnimals;
        private DarkUI.Controls.DarkCheckBox checkMonsters;
        private DarkUI.Controls.DarkComboBox comboDifficulty;
        private DarkUI.Controls.DarkLabel darkLabel6;
        private DarkUI.Controls.DarkComboBox comboGamemode;
        private DarkUI.Controls.DarkLabel darkLabel5;
        private DarkUI.Controls.DarkTextBox textMotd;
        private DarkUI.Controls.DarkLabel darkLabel7;
        private DarkUI.Controls.DarkButton buttonSaveProperties;
        private DarkUI.Controls.DarkButton buttonSaveLaunch;
        private DarkUI.Controls.DarkCheckBox checkBroadcastConsole;
        private DarkUI.Controls.DarkGroupBox darkGroupBox4;
        private DarkUI.Controls.DarkNumericUpDown numericEntity;
        private DarkUI.Controls.DarkLabel darkLabel8;
        private DarkUI.Controls.DarkCheckBox checkNether;
        private DarkUI.Controls.DarkCheckBox checkEnforceWhitelist;
        private DarkUI.Controls.DarkNumericUpDown numericPlayerLimit;
        private DarkUI.Controls.DarkLabel darkLabel12;
        private DarkUI.Controls.DarkNumericUpDown numericMaxTick;
        private DarkUI.Controls.DarkLabel darkLabel11;
        private DarkUI.Controls.DarkTextBox textPort;
        private DarkUI.Controls.DarkLabel darkLabel10;
        private DarkUI.Controls.DarkTextBox textIP;
        private DarkUI.Controls.DarkLabel darkLabel9;
        private DarkUI.Controls.DarkCheckBox checkAllowProxy;
    }
}