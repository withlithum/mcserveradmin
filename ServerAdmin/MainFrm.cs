﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkUI.Forms;
using Kajabity.Tools.Java;
using Microsoft.WindowsAPICodePack.Dialogs;
using ServerAdmin.Dialogs;
using ServerAdmin.Installer;
using ServerAdmin.Texts;

namespace ServerAdmin
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:命名样式", Justification = "<挂起>")]
    public partial class MainFrm : DarkForm
    {
        private Process serverProcess;
        private bool started;
        private readonly object syncGate = new object();
        private readonly StringBuilder output = new StringBuilder();
        private bool outputChanged;
        private readonly List<string> flushCache = new List<string>();

        public MainFrm()
        {
            InitializeComponent();
        }

        private void itemSettings_Click(object sender, EventArgs e)
        {
            new ConfigFrm().ShowDialog();
        }

        private void itemQuit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void itemVanilla_Click(object sender, EventArgs e)
        {
            new VanillaServerInstaller().ShowDialog();
        }

        private void AppendMessage(string source, string text)
        {
            textOutput.AppendText($"[{source}] {text}\r\n");
            output.Append('[').Append(source).Append("] ").AppendLine(text);
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            started = true;
            textOutput.Clear();
            output.Clear();
            AppendMessage("ServerAdmin", "正在执行服务端启动命令");

            if (!File.Exists($"server\\{Properties.Settings.Default.ServerJarName}"))
            {
                textOutput.AppendText("警告：服务端文件未找到！\r\n");
                textOutput.AppendText("请通过菜单安装服务端或者是安装自己的服务端，但请注意匹配设置中的服务端jar文件名！");
                return;
            }
            buttonStart.Enabled = false;
            itemSaveManager.Enabled = false;

            serverProcess = new Process();
            serverProcess.StartInfo.FileName = Properties.Settings.Default.JavaExecutable;
            serverProcess.StartInfo.Arguments = $"-Xmx{Properties.Settings.Default.MaxMemory}M -jar {Properties.Settings.Default.ServerJarName} nogui";
            Debug.WriteLine($"-Xmx{Properties.Settings.Default.MaxMemory}M -jar {Properties.Settings.Default.ServerJarName} nogui");
            serverProcess.StartInfo.UseShellExecute = false;
            serverProcess.StartInfo.RedirectStandardInput = true;
            serverProcess.StartInfo.RedirectStandardOutput = true;
            serverProcess.OutputDataReceived += OnOutputDataReceived;
            serverProcess.StartInfo.CreateNoWindow = true;
            serverProcess.StartInfo.WorkingDirectory = Path.Combine(Environment.CurrentDirectory, "server\\");
            Debug.WriteLine(Path.Combine(Environment.CurrentDirectory, "server\\"));
            serverProcess.Start();
            serverProcess.BeginOutputReadLine();
            serverProcess.Exited += OnProcessExited;
            buttonForceStop.Enabled = true;
            itemSettings.Enabled = false;
            Text = "[运行中] Minecraft Server Admin";
            AppendMessage("ServerAdmin", "服务端启动命令执行完毕，开始运行...");
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            lock (syncGate)
            {
                if (sender != serverProcess) return;
                output.AppendLine(e.Data);
                if (outputChanged) return;
                outputChanged = true;
                BeginInvoke(new Action(OnOutputChanged));
            }
        }

        private void OnProcessExited(object sender, EventArgs e)
        {
            lock (syncGate)
            {
                if (sender != serverProcess) return;
                serverProcess.Dispose();
                buttonForceStop.Enabled = false;
                serverProcess = null;
                Debug.WriteLine("Server stopped");
                _ = BeginInvoke(new Action(() =>
                  {
                      AppendMessage("ServerAdmin", "服务端退出");
                      started = false;
                      buttonStart.Enabled = true;
                      itemSaveManager.Enabled = true;
                      buttonForceStop.Enabled = false;
                      Text = "Minecraft Server Admin";
                      if (!this.Focused)
                      {
                          this.Flash();
                      }
                  }));
            }
        }

        private void OnOutputChanged()
        {
            lock (syncGate)
            {
                textOutput.Text = output.ToString();
                textOutput.SelectionStart = textOutput.Text.Length;
                textOutput.ScrollToCaret();
                outputChanged = false;
            }
        }

        private void ServerProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            flushCache.Add(e.Data);
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            if (!started || serverProcess == null)
            {
                textOutput.AppendText(DialogText.ServerNotRunning + "\r\n");
                darkTextBox1.Text = string.Empty;
                return;
            }

            serverProcess.StandardInput.WriteLine(darkTextBox1.Text);
            darkTextBox1.Text = string.Empty;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serverProcess?.HasExited == true)
            {
                lock (syncGate)
                {
                    if (sender != serverProcess) return;
                    serverProcess.Dispose();
                    buttonForceStop.Enabled = false;
                    serverProcess = null;
                    Debug.WriteLine("Server stopped");
                    BeginInvoke(new Action(() =>
                    {
                        AppendMessage("ServerAdmin", "服务端退出");
                        started = false;
                        buttonStart.Enabled = true;
                        itemSaveManager.Enabled = true;
                        buttonForceStop.Enabled = false;
                        Text = "Minecraft Server Admin";
                        itemSettings.Enabled = true;
                        this.Flash();
                    }));
                }
            }
        }

        private void buttonForceStop_Click(object sender, EventArgs e)
        {
            var taskDialog = new TaskDialog()
            {
                Caption = DialogText.KillServerCaption,
                Icon = TaskDialogStandardIcon.Warning,
                InstructionText = DialogText.GenericWarning,
                Text = DialogText.KillServerText,
                StandardButtons = TaskDialogStandardButtons.Yes | TaskDialogStandardButtons.No
            };

            if (taskDialog.Show() != TaskDialogResult.Yes)
            {
                return;
            }

            if (started && serverProcess?.HasExited == false)
            {
                serverProcess.Kill();
                serverProcess.Dispose();
                started = false;
                serverProcess.Dispose();
                serverProcess = null;
                buttonStart.Enabled = true;
                itemSaveManager.Enabled = true;
                buttonForceStop.Enabled = false;
                Text = "Minecraft Server Admin";
                itemSettings.Enabled = true;
            }
            if (started && (serverProcess?.HasExited != false))
            {
                started = false;
                serverProcess?.Dispose();
                serverProcess = null;
                buttonStart.Enabled = true;
                itemSaveManager.Enabled = true;
                Text = "Minecraft Server Admin";
                itemSettings.Enabled = true;
            }
        }

        private void itemAbout_Click(object sender, EventArgs e)
        {
            new About().ShowDialog();
        }

        private void itemSaveManager_Click(object sender, EventArgs e)
        {
            new SaveManager().ShowDialog();
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (started)
            {
                if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.WindowsShutDown)
                {
                    new TaskDialog()
                    {
                        Icon = TaskDialogStandardIcon.Error,
                        Caption = "MC Server Admin",
                        InstructionText = "无法退出管理器",
                        Text = "服务器还在运行。请通过发送 stop 命令终止服务端，或者强行终止服务端进程。",
                        StandardButtons = TaskDialogStandardButtons.Ok
                    }.Show();
                    e.Cancel = true;
                }
                else
                {
                    buttonForceStop.PerformClick();
                }
            }
        }

        private void itemStopCommand_Click(object sender, EventArgs e)
        {
            SubmitCommand("stop");
        }

        private void SubmitCommand(string text)
        {
            darkTextBox1.Text = text;
            buttonSubmit.PerformClick();
        }

        private void itemEnableAutoSave_Click(object sender, EventArgs e)
        {
            SubmitCommand("save-on");
        }

        private void itemReportBug_Click(object sender, EventArgs e)
        {
#pragma warning disable S1075 // URIs should not be hardcoded
            Process.Start("https://bitbucket.org/RelaperCrystal/mcserveradmin/issues/new");
#pragma warning restore S1075 // URIs should not be hardcoded
        }

        private void itemDisableAutoSave_Click(object sender, EventArgs e)
        {
            SubmitCommand("save-off");
        }

        private void itemSaveAll_Click(object sender, EventArgs e)
        {
            SubmitCommand("save-all");
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
#if !DEBUG
            buttonDebugFlash.Visible = false;
#endif
        }

        private void itemImportServer_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "打开服务器文件",
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "服务端文件|*.jar",
                Multiselect = false,
                ValidateNames = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                File.Copy(dialog.FileName, $"server\\{dialog.SafeFileName}");
                Properties.Settings.Default.ServerJarName = dialog.SafeFileName;
                Properties.Settings.Default.Save();
                MessageBox.Show("服务端文件导入成功");
            }
        }

        private void buttonDebugFlash_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            Debug.WriteLine("open minizied");
            for (int i = 0; i < 1000000; i++)
            {
                this.Update();
            }
            Debug.WriteLine("flashing");
            this.Flash();
        }
    }
}
