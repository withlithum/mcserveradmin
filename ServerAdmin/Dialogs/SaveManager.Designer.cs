﻿
namespace ServerAdmin.Dialogs
{
    partial class SaveManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonImport = new DarkUI.Controls.DarkButton();
            this.buttonDelete = new DarkUI.Controls.DarkButton();
            this.buttonBackup = new DarkUI.Controls.DarkButton();
            this.darkGroupBox1 = new DarkUI.Controls.DarkGroupBox();
            this.listFacts = new System.Windows.Forms.ListView();
            this.headerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.headerValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.darkGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(332, 341);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Padding = new System.Windows.Forms.Padding(5);
            this.buttonImport.Size = new System.Drawing.Size(154, 35);
            this.buttonImport.TabIndex = 1;
            this.buttonImport.Text = "导入存档";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(172, 341);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Padding = new System.Windows.Forms.Padding(5);
            this.buttonDelete.Size = new System.Drawing.Size(154, 35);
            this.buttonDelete.TabIndex = 2;
            this.buttonDelete.Text = "删除存档";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonBackup
            // 
            this.buttonBackup.Location = new System.Drawing.Point(12, 341);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Padding = new System.Windows.Forms.Padding(5);
            this.buttonBackup.Size = new System.Drawing.Size(154, 35);
            this.buttonBackup.TabIndex = 3;
            this.buttonBackup.Text = "备份存档";
            this.buttonBackup.Click += new System.EventHandler(this.buttonBackup_Click);
            // 
            // darkGroupBox1
            // 
            this.darkGroupBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.darkGroupBox1.Controls.Add(this.listFacts);
            this.darkGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.darkGroupBox1.Name = "darkGroupBox1";
            this.darkGroupBox1.Size = new System.Drawing.Size(480, 323);
            this.darkGroupBox1.TabIndex = 4;
            this.darkGroupBox1.TabStop = false;
            this.darkGroupBox1.Text = "概况";
            // 
            // listFacts
            // 
            this.listFacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(74)))));
            this.listFacts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.headerName,
            this.headerValue});
            this.listFacts.ForeColor = System.Drawing.Color.Gainsboro;
            this.listFacts.HideSelection = false;
            this.listFacts.Location = new System.Drawing.Point(6, 29);
            this.listFacts.Name = "listFacts";
            this.listFacts.Size = new System.Drawing.Size(468, 288);
            this.listFacts.TabIndex = 0;
            this.listFacts.UseCompatibleStateImageBehavior = false;
            this.listFacts.View = System.Windows.Forms.View.Details;
            // 
            // headerName
            // 
            this.headerName.Text = "名称";
            this.headerName.Width = 150;
            // 
            // headerValue
            // 
            this.headerValue.Text = "值";
            this.headerValue.Width = 160;
            // 
            // SaveManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 387);
            this.Controls.Add(this.darkGroupBox1);
            this.Controls.Add(this.buttonBackup);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonImport);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveManager";
            this.Text = "存档管理器";
            this.Load += new System.EventHandler(this.SaveManager_Load);
            this.darkGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DarkUI.Controls.DarkButton buttonImport;
        private DarkUI.Controls.DarkButton buttonDelete;
        private DarkUI.Controls.DarkButton buttonBackup;
        private DarkUI.Controls.DarkGroupBox darkGroupBox1;
        private System.Windows.Forms.ListView listFacts;
        private System.Windows.Forms.ColumnHeader headerName;
        private System.Windows.Forms.ColumnHeader headerValue;
    }
}