﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DarkUI.Forms;
using fNbt;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace ServerAdmin.Dialogs
{
    public partial class SaveManager : DarkForm
    {
        public SaveManager()
        {
            InitializeComponent();
        }

        private void SaveManager_Load(object sender, EventArgs e)
        {
            if (File.Exists("server\\world\\level.dat"))
            {
                var nbt = new NbtFile();
                nbt.LoadFromFile("server\\world\\level.dat");
                var dataTag = nbt.RootTag.Get<NbtCompound>("Data");
                this.Text = dataTag.Get<NbtString>("LevelName").Value + " - 存档管理器";
                
                if (dataTag.Contains("Version"))
                {
                    var versionItem = new ListViewItem("存档版本");
                    versionItem.SubItems.Add(dataTag.Get<NbtCompound>("Version").Get<NbtString>("Name").Value);
                    listFacts.Items.Add(versionItem);
                }

                var spawnX = dataTag["SpawnX"].StringValue;
                var spawnY = dataTag["SpawnY"].StringValue;
                var spawnZ = dataTag["SpawnZ"].StringValue;

                var spawnItem = new ListViewItem("出生点位置");
                spawnItem.SubItems.Add($"{spawnX}, {spawnY}, {spawnZ}");
                listFacts.Items.Add(spawnItem);
            }
            else
            {
                if (Directory.Exists("server\\world"))
                {
                    MessageBox.Show("存档文件夹下仍有剩余残留文件。请备份后删除。", "提示");
                    buttonBackup.Enabled = true;
                    buttonDelete.Enabled = true;
                    buttonImport.Enabled = false;
                }
                else
                {
                    buttonBackup.Enabled = false;
                    buttonDelete.Enabled = false;
                    buttonImport.Enabled = true;
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("警告：一旦删除存档后便永久丢失。继续吗？", "删除存档", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                Directory.Delete("server\\world", true);
                Close();
            }
        }

        private void buttonBackup_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("server\\world"))
            {
                var date = DateTime.Now;
                var backupDate = $"backups\\WorldBackup-{date.Year}-{date.Month:D2}-{date.Day:D2}-{date.Hour:D2}-{date.Minute:D2}-{date.Second:D2}-{date.Millisecond:D6}.zip";
                Directory.CreateDirectory("backups");
                ZipFile.CreateFromDirectory("server\\world", backupDate);
                MessageBox.Show("存档已备份成功。\r\n备份文件位于: " + backupDate, "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Process.Start("backups\\");
            }
        }

        private void CopyDirectory(string srcPath, string desPath)
        {
            string folderName = srcPath.Substring(srcPath.LastIndexOf("\\") + 1);
            string desfolderdir = desPath + "\\" + folderName;
            if (desPath.LastIndexOf("\\") == (desPath.Length - 1))
            {
                desfolderdir = desPath + folderName;
            }
            string[] filenames = Directory.GetFileSystemEntries(srcPath);
            foreach (string file in filenames)
            {
                if (Directory.Exists(file))
                {
                    string currentdir = desfolderdir + "\\" + file.Substring(file.LastIndexOf("\\") + 1);
                    if (!Directory.Exists(currentdir))
                    {
                        Directory.CreateDirectory(currentdir);
                    }
                    CopyDirectory(file, desfolderdir);
                }
                else
                {
                    string srcfileName = file.Substring(file.LastIndexOf("\\") + 1);
                    srcfileName = desfolderdir + "\\" + srcfileName;
                    if (!Directory.Exists(desfolderdir))
                    {
                        Directory.CreateDirectory(desfolderdir);
                    }

                    File.Copy(file, srcfileName, true);
                }
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            var common = new CommonOpenFileDialog();
            common.IsFolderPicker = true;
            common.EnsureFileExists = true;
            common.EnsurePathExists = true;
            common.AllowNonFileSystemItems = false;
            common.AllowPropertyEditing = false;
            common.Multiselect = false;

            if (common.ShowDialog() == CommonFileDialogResult.Ok && File.Exists(Path.Combine(common.FileName, "level.dat")))
            {
                var dialog = new TaskDialog();
                dialog.Caption = "导入存档";
                dialog.Text = "请使用与游戏版本相匹配的存档，否则出现问题需删除服务器存档。\r\n因本功能存在一定风险，请酌情使用本功能。\r\n是否继续？";
                dialog.InstructionText = "警告";
                dialog.Icon = TaskDialogStandardIcon.Warning;
                dialog.StandardButtons = TaskDialogStandardButtons.Yes | TaskDialogStandardButtons.No;
                if (dialog.Show() != TaskDialogResult.Yes)
                {
                    return;
                }

                CopyDirectory(common.FileName, "server\\");
                string folderName = Path.GetFileName(common.FileName);
                Directory.Move("server\\" + folderName, "server\\world");
                MessageBox.Show("导入成功");
                Close();
            }
        }
    }
}
