﻿using System;
using System.Reflection;
using System.Security.Policy;
using System.Windows.Forms;
using DarkUI.Controls;
using DarkUI.Forms;

namespace ServerAdmin.Dialogs
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S1186:Methods should not be empty", Justification = "<挂起>")]
    public partial class About : DarkForm
    {
        public About()
        {
            InitializeComponent();
            labelVersion.Text += Assembly.GetExecutingAssembly().GetName().Version;
        }

#pragma warning disable IDE1006 // 命名样式
        private void darkButton1_Click(object sender, System.EventArgs e)
#pragma warning restore IDE1006 // 命名样式
        {
            Close();
        }

#pragma warning disable IDE1006 // 命名样式
        private void buttonLicense_Click(object sender, System.EventArgs e)
#pragma warning restore IDE1006 // 命名样式
        {
            var form = new DarkForm()
            {
                Text = "许可证",
                Icon = this.Icon
            };
            var textBox = new DarkTextBox
            {
                ReadOnly = true,
                Text = Properties.Resources.License.Replace("\n", "\r\n"),
                Multiline = true,
                Dock = DockStyle.Fill,
                ScrollBars = ScrollBars.Vertical
            };
            this.AddOwnedForm(form);
            form.Controls.Add(textBox);
            form.Height = (int)(this.Height * 1.5);
            form.Width = (int)(this.Width * 1.35);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.MaximizeBox = false;
            form.MinimizeBox = false;
            form.ShowInTaskbar = false;
            form.ShowDialog();
        }
    }
}
