﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerAdmin.Api
{
    /// <summary>
    /// Represents a user plugin.
    /// </summary>
    public interface IUserPlugin
    {
        /// <summary>
        /// Called when this instance has been initialized.
        /// </summary>
        void Initialize();
    }
}
